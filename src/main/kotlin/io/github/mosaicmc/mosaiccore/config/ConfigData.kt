package io.github.mosaicmc.mosaiccore.config

/**
 * Represents a config object.
 */
interface ConfigData